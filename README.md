CAB302 Software Development
===========================

# Practical 5: Patterns and Refactoring

The exercises for this week are all about taking patterns and using them, or taking bad code and improving it. We will begin with patterns.

## Exercise 1: Understanding the Mystery

The Mystery Pattern example is provided in the practical materials in the package `patterns.mystery`.
We are going to explore the code.
Note the changes to the constructor to allow us to pass a string argument, which becomes the internal state of the object.

- Refactor: Use the facilities provided by your IDE to rename the class `Mystery` to `Singleton`.
But do this in stages, using the refactoring option on the right click menu.
Don't just do a global replace.
- Run the code as a Java application and explain what is significant about the trivial result observed.
- Remove the access modifier protected from the constructor. Run the program again. What happens?
Replace it with private. Run the program again, and see what happens. Why do we choose protected?

## Exercise 2: Observer Pattern in Java

In the lecture we looked at the Observer pattern and a variation on its implementation.
The Observer pattern is a commonly used pattern, and in the past Java provided its own version.
Look at the APIs for [`java.util.Observer`](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Observer.html) and [`java.util.Observable`](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Observable.html).
Then modify the Temperature application (in the package `applications.temperature`) to use `java.util.Observer` and `java.util.Observable` from the JDK instead of the provided `patterns.observer.Observer` and `patterns.observer.Subject`.

Note that IntelliJ will put a strike through the names 'Observer' and 'Observable' due to those classes being *deprecated* in Java 11. If this annoys you, you can disable these warnings by clicking on the yellow lightbulb and disabling the notification.

For a more modern implementation of the Observer pattern in the JDK, check out [`java.beans.PropertyChangeSupport`](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/java/beans/PropertyChangeSupport.html) and [`java.beans.PropertyChangeListener`](https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/java/beans/PropertyChangeListener.html).

## Exercise 3: A Pseudo GUI

In this exercise we are going to simulate a GUI application.
This is purely for teaching purposes, although the code will look very similar to code I've worked with inside a custom Java GUI widget.
The difference is that our classes are not going to inherit down a long chain of GUI classes.
To avoid name clashes, we are not going to use real Swing and AWT GUI Listener interfaces, but rather we are going to put the letters gui in front of everything.
Similarly, we are not going to copy exactly the interface from the lecture, but you should use the Observer/Subject example code as a very good guide to the implementation needed.
Methods will not have the same names, but will be directly analogous to those in the pattern as presented in the lecture.
You can work it out from there.

Of course, this exercise will be much less complex than the real thing.
Those who want to explore how it is done in Java should see the appendix at the end of this document.
Needless to say, that material will NOT be examinable.
In that case, the event control takes place within a class separate from the button called a `ButtonModel`.
We will cheat a little and control everything from a `doClick()` method.
The button has a private field indicating whether it has been clicked.
In the `doClick()` method, set it to true, then fire the events and then restore it to false.

In the `guiSkeleton` package you will find two sub-packages, one containing the GUI interfaces and event class:

- `GUIEventSource.java` – interface (Subject)
- `GUIEventListener.java` – interface (Observer)
- `GUIEvent.java` – Event class

and the other containing cut down versions of the required classes:

- `GUIButton.java` – the pseudo GUI component
- `GUIDisplay.java` – the GUI regions which listen for events
- `GUISimulator.java` – the control object and main program

Note that both the Button and Display classes have many gap sections indicated by 'XXXXXX'.
Obviously the code isn't going to compile, and you need to replace these with sensible code to
get anywhere. The simulator class is left intact, and in many respects is a properly structured
version of the code that you saw in the lecture.

Your main task is to implement the incomplete methods based on your knowledge of the Observer Pattern.
The `guiActionPerformed` method is tricky, as it takes a `GUIEvent` object, so I have left that one in place.

You are required to use the event and event source objects to cause the display to have the form:

    Left Hand Panel:GUIEvent:guiButton:OK:2012-04-11 21:29:18

The actual code to be written is not very complex, but it will take you some time to get to that point.
Take your time, and make sure you can work your way through the code successfully.
The key is to recognise that the `GUIEvent` object carries a lot of information, including the source object which generated the event, and this is readily accessible via public methods.
Use intellisense (actually that is the Microsoft term) or 'quick-assist' and/or the Java API docs to help you.

Special instructions for the `doClick()` method: it is usual in these sorts of methods to change the state of the object in the first line, do something in the middle line(s) and then to reset the state at the end.
[That's enough of a hint for a three line method.]
Run the code, and see that you get responses of this type from the two panels.
Now modify the code as follows:

- Add a new display called "Centre Panel".
Use the refactoring facilities of eclipse to modify the methods in the main program.
Run to confirm that you now have three listeners providing a response.
If you don't see a Centre Panel entry, you forgot to wire it up.
- Now modify the `guiActionPerformed` method so that it changes the name of the button each time we simulate a click.
Get started by making a local copy – within the method – of the button object, then use the setter to add a "K" to the name each time we make the call.
- Optional: For those adventurous enough, add a field to the `GUIEvent` class to hold the time at which the event was created (you will need a new constructor) and then print this out in the `guiActionPerformed` method.

## Exercise 4: Refactoring

Unit Test code is almost always ugly, poorly structured, anything but self-documenting and
repetitive. So refactor it.
Take at least one class from your first assignment, and greatly improve it... Among other things, you should:

- Rename
- Encapsulate conditionals in methods
- Extract methods from endless line after line code
- Get rid of magic numbers (Extract constant)

Make sure that you re-run your tests after each refactoring to ensure that the behaviour of the code remains the same.

## APPENDIX: How the Buttons Actually Work

Those interested in how Java really manages all this stuff should go to the source, so to speak:

[http://www.docjar.com/html/api/javax/swing/AbstractButton.java.html](https://web.archive.org/web/20160417101221/http://www.docjar.com/html/api/javax/swing/AbstractButton.java.html)

This is the source code (note the Oracle copyright notice at the top of the file) for the `AbstractButton` class.
This specifies most of the behaviour of buttons like `JButton`.
Have a look at the `doClick` method at line 367 which shows some of the elementary events which control our response to a click.
In a real GUI, these are organised in response to low-level mouse events.
Here is a description from the `ButtonModel` header comment (see below):

*In details, the state model for buttons works as follows when used with the mouse:
Pressing the mouse on top of a button makes the model both armed and pressed. As
long as the mouse remains down, the model remains pressed, even if the mouse moves
outside the button. On the contrary, the model is only armed while the mouse remains
pressed within the bounds of the button (it can move in or out of the button, but the
model is only armed during the portion of time spent within the button). A button is
triggered, and an `ActionEvent` is fired, when the mouse is released while the model
is armed - meaning when it is released over top of the button after the mouse has
previously been pressed on that button (and not already released). Upon mouse
release, the model becomes unarmed and unpressed.*

See line 1918 for `addActionListener` and shortly after to remove the listener.
Finally, at line 1995, you'll see the code for firing an `ActionEvent`.
Note that the code here is a little confusing – in part because the button is handling its own events, and because some of the necessary event 'wiring' is supplied elsewhere.

The part you are missing is the `ButtonModel`, which is an interface which specifies a model of the button state and provides the scaffolding for event processing.
There are a number of standard implementations of this interface, but we will work with the default:

[http://www.docjar.com/html/api/javax/swing/DefaultButtonModel.java.html](https://web.archive.org/web/20160417094903/http://www.docjar.com/html/api/javax/swing/DefaultButtonModel.java.html)

The key code will be found at line 293, `fireActionPerformed`, which is directly analogous to `notifyObservers` of the Observer pattern.
Then, in some rather ugly code at line 385, we see the event actually fired. The sequence is roughly something like this:

1. In `doClick()` or by using the mouse, we press and release the button. In either approach, we also cause the button to become armed. For simplicity, we will work only with the sequence laid out in `doClick()`.
2. The `setPressed(false)` call at the end of the `doClick()` method changes the pressed state, but does so while the button is still armed.
Hence the conditional at line 402 of this method is true and so we fire the event to all the registered listeners.